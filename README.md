# Application Description

This application was build to meet requirement of tokopedia test.
Build with Create React App, using react hooks and react context as global state management.

You can access demo [here](https://tokopedia-pokedex.vercel.app)

## Installation

1. git clone https://gitlab.com/rizky.rachmawan/tokopedia-pokedex.git
2. npm install

## Development

1. npm run start
2. open your browser at localhost:3000

## Development Build
1. npm run build


## Testing
1. npm run test:cover
2. Open file from directory `./coverage/lcov-report/index.html` in browser
