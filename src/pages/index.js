import React, { lazy, Suspense } from 'react';
import Loading from '../components/elements/Loading/Loading';

const Suspensed = (Element) => function suspense(props) {
  return (
    <Suspense fallback={<Loading />}>
      <Element {...props} />
    </Suspense>
  );
};

const pages = {
  Home: Suspensed(lazy(() => import('./Home'))),
  Detail: Suspensed(lazy(() => import('./Detail'))),
  MyPokemon: Suspensed(lazy(() => import('./MyPokemon'))),
}

export default pages;
