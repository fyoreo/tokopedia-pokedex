import React, { useContext } from 'react';
import Title from '../../components/elements/Title'
import { URL } from '../../config'
import Card from '../../components/elements/Card';
import CardFooter from '../../components/elements/CardFooter';
import CardBody from '../../components/elements/CardBody';
import styles from './styles.module.css';
import { PokemonContext } from '../../context/pokemonContext';
import Button from '../../components/elements/Button';
import Image from '../../components/elements/Image';
import { Link } from 'react-router-dom';
import { ROUTES } from '../../config'
import CardHeader from '../../components/elements/CardHeader';

const MyPokemon = () => {
  const { pokemonPersist, dispatch } = useContext(PokemonContext);
  const pokemonList = () => {
    return pokemonPersist.myPokemon.map((val, idx) => {
      return (
        <div className={styles['card-container']} key={idx}>
          <Link key={idx} to={`${ROUTES.POKEMON}/${val.name}`}>
            <Card className={styles['item-card']}>
              <CardHeader>
                <span className={styles.name}>{val.name}</span>
              </CardHeader>
              <CardBody>
                <div className={styles['item-content']}>
                  <Image url={`${URL.IMAGE_URL}${val.pokeId}.png`} alt={val.name} />
                </div>
              </CardBody>
              <CardFooter className={styles['item-footer']}>
                <div>
                  <span> {val.givenName}</span>
                </div>
              </CardFooter>
            </Card>
          </Link>
          <div>
            <Button type="button" onClick={() => { handleRelease(val.id) }} className={styles['btn-release']}>
              release !
            </Button>
          </div>
        </div>
      )
    })
  }

  const handleRelease = (id) => {
    const r = window.confirm("Are you sure wanna release this pokemon ?");
    if (r === true) {
      const payload = pokemonPersist.myPokemon.filter((val) => {
        return val.id !== id
      })
      dispatch({ type: "RELEASE_POKEMON", payload });
    }
  }

  return (
    <div className={styles.root}>
      <Title>
        My Pokémon List
      </Title>
      <div className={styles['item-container']}>
        {pokemonList()}
      </div>
    </div>
  )
}


export default MyPokemon