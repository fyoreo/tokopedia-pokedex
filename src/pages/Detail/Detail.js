import React, { useState, useEffect, useContext } from 'react';
import { PokemonContext } from "../../context/pokemonContext";
import { fetchPokemonDetailComplete } from './action'
import { URL } from '../../config'
import Card from '../../components/elements/Card';
import styles from './styles.module.css';
import Title from '../../components/elements/Title';
import Ability from '../../components/fragments/Detail/Ability/Ability';
import Type from '../../components/fragments/Detail/Type';
import Move from '../../components/fragments/Detail/Move/Move';
import Stat from '../../components/fragments/Detail/Stat';
import { uuid } from '../../utils/helper';
import SpriteImage from '../../components/fragments/Detail/SpriteImage/SpriteImage';
import Evolution from '../../components/fragments/Detail/Evolution';
import Button from '../../components/elements/Button';
import FormCatchPokemon from '../../components/forms/FormCatchPokemon';
import Image from '../../components/elements/Image';

const Detail = (props) => {
  const { id } = props.match.params;
  const [pokemonDetail, setPokemonDetail] = useState({});
  const [own, setOwn] = useState(0);
  const [pokemonEvolution, setPokemonEvolution] = useState([]);
  const [pokemonName, setPokemonName] = useState('');
  const [catchPokemon, setCatchPokemon] = useState(false);
  const [error, setError] = useState('');
  const [message, setMessage] = useState('');
  const { pokemonPersist, dispatch } = useContext(PokemonContext);

  useEffect(() => {
    const fetchData = async () => {
      const [detail, species, evolution] = await fetchPokemonDetailComplete(id)
      setPokemonDetail(detail.data)
      handleGetEvolutionChain(evolution.data)
    };
    fetchData();
  }, [id]);

  useEffect(() => {
    handleCheckOwning()
  }, [pokemonDetail]);

  const handleGetEvolutionChain = (result) => {
    let modData = result.chain || {}
    let evoChain = [];
    if (Object.keys(modData).length > 0)
      do {
        evoChain.push({ "species_name": modData.species.name, });
        modData = modData['evolves_to'][0];
      } while (!!modData && modData.hasOwnProperty('evolves_to'));
    setPokemonEvolution(evoChain)
  }

  const handleCheckOwning = () => {
    let owned = 0
    if (Object.keys(pokemonDetail).length !== 0) {
      owned = pokemonPersist.myPokemon.filter((el) => {
        return el.name === pokemonDetail.name
      });
    }
    setOwn(owned.length)
  }

  const handleCatch = () => {
    const chance = Math.floor(Math.random() * 100) + 1;
    const catchPokemon = chance > 50 ? true : false;
    if (catchPokemon) {
      setCatchPokemon(true)
      setMessage('')
    }
    else {
      setMessage(`Ooppps.. ${pokemonDetail.name} has run away ...`)
      setTimeout(function () { setMessage(``) }, 3000);
    }
  }

  const handleSavePokemon = (e) => {
    e.preventDefault();
    const { id, name } = pokemonDetail;
    const payload = { id: uuid(), pokeId: id, name: name, givenName: pokemonName }
    const isExistingName = handleCheckName(pokemonName)
    if (!isExistingName) {
      dispatch({ type: "CATCH_POKEMON", payload });
      setMessage(`Success Catch ${pokemonDetail.name}`)
      handleReset()
      handleSetOwningPersistant()
      setOwn(own + 1)
    } else {
      setError('name already taken / name cannot be empty')
    }
  }

  const handleSetOwningPersistant = () => {
    const listPokemon = pokemonPersist.listPokemon.map((el) => {
      if (el.name === pokemonDetail.name) {
        return Object.assign({},
          { ...el, owned: own + 1 }
        )
      } else {
        return el
      }
    });
    dispatch({ type: "LIST_POKEMON", listPokemon });
  }


  const handleCheckName = (name) => {
    const isExistingName = pokemonPersist.myPokemon.some(val => {
      return val.givenName === name || !name ? true : false
    })
    return isExistingName
  }

  const handleReset = () => {
    setPokemonName('')
    setError('')
    setCatchPokemon(false)
    setTimeout(function () { setMessage(``) }, 3000);
  }

  if (Object.keys(pokemonDetail).length === 0) { return <div>Loading</div> }
  const { abilities, sprites, types, moves, stats } = pokemonDetail;
  return (
    <div className={styles.root}>
      <section className={styles['item-detail']}>
        <div className={styles['image-container']}>
          <Card className={styles['item-image']}>
            <Image url={`${URL.IMAGE_URL}${pokemonDetail.id}.png`} alt={pokemonDetail.name} />
          </Card>
          <SpriteImage sprites={sprites} />
        </div>
        <div className={styles['item-info']}>
          <Title heading="h1" className={styles['item-info-title']}>
            {pokemonDetail.name}<span className={styles.own}> owned ({own})</span>
          </Title>
          <Type types={types} />
          <Stat stats={stats} />
          <Ability abilities={abilities} />
          <Evolution evolutions={pokemonEvolution} />
          <div className={styles['btn-container']}>
            <Button onClick={() => handleCatch()} disabled={catchPokemon}>
              Catch Pokemon !
            </Button>
            <span className={styles['message-info']}>{message}</span>
          </div>
        </div>
      </section >
      <FormCatchPokemon
        catchPokemon={catchPokemon}
        error={error}
        handleSavePokemon={handleSavePokemon}
        handleReset={handleReset}
        setPokemonName={setPokemonName}
        pokemonDetail={pokemonDetail}
        pokemonName={pokemonName}
      />
      <div>
        <Move moves={moves} />
      </div>
    </div >
  )
}

export default Detail