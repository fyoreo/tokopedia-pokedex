import React from 'react';
import { render } from 'react-dom';

const mockComponent = (name, props) => <div className={name} {...props} />;

jest.mock('../App', () => ({
  __esModule: true,
  default: props => mockComponent('App', props),
}));
jest.mock('react-dom');

describe('src/index', () => {
  test('render', () => {
    expect(render).toHaveBeenCalledTimes(0);
  });
});
