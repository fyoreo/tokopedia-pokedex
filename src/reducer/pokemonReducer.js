export const pokemonReducer = (state, action) => {
  switch (action.type) {
    case "CATCH_POKEMON":
      return {
        ...state,
        myPokemon: [...state.myPokemon, action.payload]
      }
    case "RELEASE_POKEMON":
      return {
        ...state,
        myPokemon: action.payload
      };
    case "LIST_POKEMON":
      return {
        ...state,
        listPokemon: action.listPokemon
      };
    default:
      return state;
  }
};