import { ROUTES } from '../config'

const listMenu = [
  {
    path: ROUTES.HOME,
    name: 'Pokédex'
  },
  {
    path: ROUTES.MY_POKEMON,
    name: 'My Pokémon '
  },
];

export default listMenu;
