import routes from './routes';
import images from './images';
import url from './url'

export const ROUTES = routes;
export const IMAGES = images;
export const URL = url;
