import React from 'react';
import Images from '../Images';
import renderer from 'react-test-renderer';

describe('Images component', () => {
  let props = {};
  beforeEach(() => {
    props = {};
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<Images {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
