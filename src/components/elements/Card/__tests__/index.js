import React from 'react';
import Card from '../Card';
import renderer from 'react-test-renderer';

describe('Card component', () => {
  let props = {};
  beforeEach(() => {
    props = {};
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<Card {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
