import PropTypes from 'prop-types';
import styles from './styles.module.css';

const CardBody = (props) => {
  const modStyle = [styles.root, props.className].join(' ');
  return (
    <div className={modStyle}>
      {props.children}
    </div>
  )
}

export default CardBody

CardBody.defaultProps = {
  children: null,
  className: '',
};

CardBody.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
