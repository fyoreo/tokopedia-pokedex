import React from 'react';
import CardBody from '../CardBody';
import renderer from 'react-test-renderer';

describe('CardBody component', () => {
  let props = {};
  beforeEach(() => {
    props = {};
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<CardBody {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
