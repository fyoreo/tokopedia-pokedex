import React from 'react';
import CardHeader from '../CardHeader';
import renderer from 'react-test-renderer';

describe('CardHeader component', () => {
  let props = {};
  beforeEach(() => {
    props = {};
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<CardHeader {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
