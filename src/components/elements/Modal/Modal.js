import styles from './styles.module.css';
import React, { useContext } from 'react';
import { ModalContext } from '../../../context/modalContext';

const Modal = (props) => {
  const { children } = props
  const { modal } = useContext(ModalContext);
  const modModalStyle = modal.isActive ? [styles.root, styles.active].join(' ') : styles.root

  return (
    <div className={modModalStyle}>
      {children}
    </div>
  )
}

export default Modal