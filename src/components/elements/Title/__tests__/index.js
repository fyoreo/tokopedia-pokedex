import React from 'react';
import Title from '../Title';
import renderer from 'react-test-renderer';

describe('Title component', () => {
  let props = {};
  beforeEach(() => {
    props = {};
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<Title {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
