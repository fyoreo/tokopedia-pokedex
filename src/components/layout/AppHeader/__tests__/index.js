import React from 'react';
import AppHeader from '../AppHeader';
import renderer from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';

describe('AppHeader component', () => {
  let props = {};
  beforeEach(() => {
    props = {};
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(
        <BrowserRouter>
          <AppHeader {...props} />
        </BrowserRouter>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
