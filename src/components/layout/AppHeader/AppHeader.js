import React, { useState, useEffect } from 'react';
import styles from './styles.module.css';
import ListMenu from '../../../constants/ListMenu'
import { Link } from 'react-router-dom';
import { IMAGES, ROUTES } from '../../../config';
import IconAppDrawer from '../../Icon/AppDrawer'
import IconArrowRight from '../../Icon/ArrowRight'

const AppHeader = () => {
  const [appDrawer, setAppDrawer] = useState(false);
  const modStyle = appDrawer ? [styles.collapse, styles.active].join(' ') : styles.collapse;

  useEffect(() => {
    const overflow = !appDrawer ? 'auto' : 'hidden';
    document.body.style.overflow = overflow
  }, [appDrawer]);

  const menu = ListMenu.map((val, index) => {
    return (
      <Link to={val.path} key={index} onClick={() => setAppDrawer(!appDrawer)}>
        <span className={styles['nav-icon']}>
          <IconArrowRight />
        </span>
        {val.name}
      </Link>
    )
  })

  return (
    <header>
      <div className={styles.root}>
        <div className={styles['nav-container']}>
          <Link to={ROUTES.HOME} onClick={() => setAppDrawer(!appDrawer)}>
            <div className={styles['nav-brand']}>
              <img src={IMAGES.TOKOPEDIA_LOGO} alt="tokopedia logo" className={styles['nav-brand-logo']} width="100%" height="100%" />
              <span>Tokopedia Pokémon Assignment</span>
            </div>
          </Link>
          <div className={styles.drawer}>
            <button aria-label="app-drawer" onClick={() => setAppDrawer(!appDrawer)} >
              <IconAppDrawer />
            </button>
          </div>
        </div>
        <div className={styles.navigation}>
          {menu}
        </div>
        <div className={modStyle}>
          {menu}
        </div>
      </div>
    </header>
  )
}

export default AppHeader