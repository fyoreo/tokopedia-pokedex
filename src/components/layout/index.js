import React, { useEffect, useContext } from 'react';
import { ModalContext } from '../../context/modalContext';
import AppHeader from './AppHeader';
import AppMain from './AppMain';
import styles from './styles.module.css';

const Layout = (props) => {
  // const { modal } = useContext(ModalContext);
  // const modModalStyle = modal.isActive ? [styles.overlay, styles.active].join(' ') : styles.overlay
  // const ModalContent = () => {
  //   return modal && modal.modalContent && modal.modalContent.props && modal.modalContent.props.children
  // }
  // useEffect(() => {
  //   modal.isActive ? document.body.style.overflow = 'hidden' : document.body.style.overflow = 'auto'
  // }, [modal]);
  
  return (
    <>
      {/* <div className={modModalStyle}>
        <div className={styles.modal}>
          {ModalContent()}
        </div>
      </div> */}
      <AppHeader />
      <AppMain>
        {props.children}
      </AppMain>
    </>
  )
}

export default Layout