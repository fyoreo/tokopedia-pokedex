import React from 'react';
import Layout from '../index';
import renderer from 'react-test-renderer';
import { BrowserRouter } from 'react-router-dom';

describe('Layout component', () => {
  let props = {};
  beforeEach(() => {
    props = {};
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<BrowserRouter><Layout {...props} /></BrowserRouter>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
