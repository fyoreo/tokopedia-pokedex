import styles from './styles.module.css';

const AppMain = (props) => {
  return (
    <main>
      <div className={styles.root}>
        {props.children}
      </div>
    </main>
  )
}

export default AppMain