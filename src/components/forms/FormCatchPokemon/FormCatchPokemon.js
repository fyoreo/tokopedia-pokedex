import Title from '../../elements/Title'
import Button from '../../elements/Button'
import styles from './styles.module.css';

const FormCatchPokemon = (props) => {
  const { catchPokemon, pokemonDetail, handleSavePokemon, setPokemonName, pokemonName, error, handleReset } = props
  const modStyle = catchPokemon ? [styles.root, styles.active].join(' ') : styles.root
  return (
    <div className={modStyle}>
      <Title heading={'h4'} className={styles['catch-info']}>
        Yeaay .. You catch {pokemonDetail.name}<br />
        Please give a name your pokemon
      </Title>
      <form onSubmit={handleSavePokemon} className={styles['form-container']}>
        <input type="text" name="pokemonName" onChange={(e) => setPokemonName(e.target.value)} placeholder="Enter your pokemon name" value={pokemonName} />
        <span className={styles.error}>
          {error}
        </span>
        <div>
          <Button type="submit">
            Give a name !
          </Button>
          <Button type="button" onClick={() => handleReset()}>
            Release
          </Button>
        </div>
        <br />
      </form>
    </div>
  )
}

export default FormCatchPokemon;