import React from 'react';
import FormCatchPokemon from '../FormCatchPokemon';
import renderer from 'react-test-renderer';

describe('FormCatchPokemon component', () => {
  let props = {};
  beforeEach(() => {
    props = {
      pokemonDetail:{
        name:''
      }
    };
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<FormCatchPokemon {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
