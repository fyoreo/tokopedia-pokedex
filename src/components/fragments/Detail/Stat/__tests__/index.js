import React from 'react';
import Stat from '../Stat';
import renderer from 'react-test-renderer';

describe('Stat component', () => {
  let props = {};
  beforeEach(() => {
    props = {
      stats:[]
    };
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<Stat {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
