import React from 'react';
import Move from '../Move';
import renderer from 'react-test-renderer';

describe('Move component', () => {
  let props = {};
  beforeEach(() => {
    props = {
      moves: [
        {
          move: {
            name: '',
          }
        }
      ]
    };
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<Move {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
