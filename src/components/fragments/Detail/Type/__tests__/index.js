import React from 'react';
import Type from '../Type';
import renderer from 'react-test-renderer';

describe('Type component', () => {
  let props = {};
  beforeEach(() => {
    props = {
      types:[]
    };
  });

  it('renders correctly when loading', () => {
    props.isLoading = true;
    const tree = renderer
      .create(<Type {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
