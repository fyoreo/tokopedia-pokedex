import styles from './styles.module.css';

const SpriteImage = (props) => {
  const { sprites } = props;
  const sortSprites = Object.keys(sprites).sort().reverse();

  return (
    <div className={styles['item-image-other']}>
      {sortSprites.map((val, idx) => {
        if (typeof sprites[val] === 'string')
          return (
            <div key={idx}>
              <img src={sprites[val]} alt={val} title={val} />
            </div>
          )
      })}
    </div>
  )
}

export default SpriteImage;