import React from 'react';
import PropTypes from 'prop-types';

export default function AppDrawer({ className }) {

  return (
    <svg width="6" height="12" viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1.78087 0.375305C1.43586 -0.0559568 0.806567 -0.125878 0.375305 0.219131C-0.0559569 0.56414 -0.125878 1.19343 0.219131 1.62469L1.78087 0.375305ZM5 6.00001L5.78087 6.6247C6.07304 6.25948 6.07304 5.74053 5.78087 5.37531L5 6.00001ZM0.219132 10.3753C-0.125878 10.8066 -0.0559567 11.4359 0.375305 11.7809C0.806567 12.1259 1.43586 12.056 1.78087 11.6247L0.219132 10.3753ZM0.219131 1.62469L4.21913 6.6247L5.78087 5.37531L1.78087 0.375305L0.219131 1.62469ZM4.21913 5.37531L0.219132 10.3753L1.78087 11.6247L5.78087 6.6247L4.21913 5.37531Z" fill="#333333" />
    </svg>  
  );
}

AppDrawer.defaultProps = {
  className: ''
};

AppDrawer.propTypes = {
  className: PropTypes.string
};
