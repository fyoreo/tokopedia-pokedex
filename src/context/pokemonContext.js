import React, { createContext, useEffect, useReducer } from "react";
import REDUCER from '../reducer'
import usePersistedState from '../persist'

export const PokemonContext = createContext();

const initilalValue = {
  myPokemon: [],
  listPokemon: [],
}

const PokemonProvider = ({ children }) => {
  const [pokemonPersist, setPokemonPersist] = usePersistedState('pokemon', initilalValue);
  const [pokemon, dispatch] = useReducer(REDUCER.POKEMON_REDUCER, pokemonPersist);

  useEffect(() => {
    setPokemonPersist(pokemon)
  }, [pokemon]);

  return (
    <PokemonContext.Provider value={{ pokemonPersist, dispatch }}>
      {children}
    </PokemonContext.Provider>
  );
};

export default PokemonProvider;
