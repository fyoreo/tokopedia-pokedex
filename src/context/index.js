const { default: ModalProvider } = require("./modalContext")
const { default: PokemonProvider } = require("./pokemonContext")

const ContextProvider = (props) => {
  const { children } = props;
  return (
    <ModalProvider>
      <PokemonProvider>
        {children}
      </PokemonProvider>
    </ModalProvider>
  )
}

export default ContextProvider